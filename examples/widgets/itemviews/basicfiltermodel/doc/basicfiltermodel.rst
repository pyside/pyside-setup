Basic Sort/Filter Model Example
===============================

The Basic Sort/Filter Model example illustrates how to use `QSortFilterProxyModel`
to perform basic sorting and filtering. This example demonstrates the analogous
Qt example `Basic Sort/Filter Model Example
<https://doc.qt.io/qt-6/qtwidgets-itemviews-basicsortfiltermodel-example.html>`_.

.. image:: basicfiltermodel.webp
    :width: 400
    :alt: basicsortfiltermodel screenshot
