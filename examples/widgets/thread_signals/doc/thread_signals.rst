Thread Signals Examples
=======================

This example demonstrates a PySide6 application that uses threads and signals
to perform background tasks.

.. image:: thread_signals.png
    :width: 400
    :alt: thread_signals screenshot
