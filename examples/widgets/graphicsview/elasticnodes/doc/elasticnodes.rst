Elastic Nodes Example
=====================

This example demonstrates a PySide6 application that creates a dynamic and
interactive graphical scene with nodes connected by elastic edges. The nodes
and edges respond to user interactions and physics-based movements, showcasing
the use of `QGraphicsItem`, `QGraphicsScene`, and `QGraphicsView` for creating
complex animations and interactions.

It demonstrates the analogous Qt example `Elastic Nodes Example
<https://doc.qt.io/qt-6/qtwidgets-graphicsview-elasticnodes-example.html>`_.

.. image:: elasticnodes.png
    :width: 400
    :alt: elasticnodes screenshot
