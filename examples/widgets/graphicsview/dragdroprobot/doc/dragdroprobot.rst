Drag and Drop Robot Example
===========================

This example demonstrates a PySide6 application where users can drag and drop
color items onto a robot graphic, showcasing interactive drag-and-drop
functionality within a `QGraphicsView`. It demonstrates the analogous Qt example
`Drag and Drop Robot Example
<https://doc.qt.io/qt-6/qtwidgets-graphicsview-dragdroprobot-example.html>`_.

.. image:: dragdroprobot.png
    :width: 400
    :alt: dragdroprobot screenshot
