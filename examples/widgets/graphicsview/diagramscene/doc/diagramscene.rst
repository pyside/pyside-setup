Diagram Scene Example
=====================

This example demonstrates how to create an interactive diagram editor using
PySide6, featuring various diagram shapes, connectors, and customizable
properties. It demonstrates the analogous Qt example `Diagram Scene Example
<https://doc.qt.io/qt-6/qtwidgets-graphicsview-diagramscene-example.html>`_.

.. image:: diagramscene.webp
    :width: 400
    :alt: diagramscene screenshot
