Standard Dialogs Example
========================

The Standard Dialogs example shows the standard dialogs that are provided by
Qt.

.. image:: standarddialogs.png
   :align: center
