Extension Example
=================

This example demonstrates a PySide6 application that creates a find dialog with
an extendable interface, showcasing how to add and manage additional options
dynamically within a dialog. It demonstrates the analogous Qt example
`Extension Example <https://doc.qt.io/qt-6.2/qtwidgets-dialogs-extension-example.html>`_.

.. image:: extension.png
    :width: 400
    :alt: extension screenshot
