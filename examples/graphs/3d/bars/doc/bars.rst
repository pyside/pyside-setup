Simple Bar Graph
================

The Qt 3D Bar Graph example demonstrates creating a 3D bar graph in QML
using Bars3D. It visualizes fictional company data for income and expenses
over time, showcasing features like data series switching, custom axis labels,
and interactive data selection.


.. image:: bars-example.webp
   :width: 400
   :alt: Widget Screenshot
