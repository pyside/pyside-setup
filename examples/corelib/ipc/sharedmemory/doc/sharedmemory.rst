IPC: Shared Memory
==================

Demonstrates how to share image data between different processes using the
Shared Memory IPC mechanism. It corresponds to the Qt example
`Shared Memory Example <https://doc.qt.io/qt-6/qtcore-ipc-sharedmemory-example.html>`_.

.. image:: sharedmemory.png
    :align: center
    :alt: sharedmemory screenshot
    :width: 400
