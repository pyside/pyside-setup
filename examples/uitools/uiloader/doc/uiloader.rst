UILoader Example
================

This example demonstrates how to dynamically load and display a user interface
designed with Qt Designer using PySide6's `QUiLoader`. It shows how to load a
`.ui` file at runtime, allowing for flexible and dynamic UI design and
modification without recompiling the application.
