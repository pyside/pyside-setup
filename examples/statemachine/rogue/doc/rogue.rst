StateMachine Rogue Example
==========================

The Rogue example shows how to use the Qt state machine for event handling
It corresponds to the Qt example `Rogue Example
<https://doc.qt.io/qt-5/qtwidgets-statemachine-rogue-example.html>`_.

.. image:: rogue.png
    :align: center
    :alt: rogue screenshot
    :width: 400
