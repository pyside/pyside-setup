Blocking Fortune Client Example
===============================

Demonstrates how to create a client for a network service. It corresponds to the
Qt example `Blocking Fortune Client Example
<https://doc.qt.io/qt-6/qtnetwork-blockingfortuneclient-example.html>`_.

.. image:: blockingfortuneclient.png
    :align: center
    :alt: blockingfortuneclient screenshot
    :width: 400
